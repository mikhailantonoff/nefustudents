// GITlab5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "chocolate.h"
#include <iostream>
#include <stdio.h>


int _tmain(int argc, _TCHAR* argv[])
{
	class chocolate a,b,c;
	a.name="mars";
	a.weight=50;
	a.with_nuts=0;
	b.name="snickers";
	b.weight=100;
	b.with_nuts=1;
	c.name="twix";
	c.weight=82;
	c.with_nuts=0;

	cout << "Shoko "<< a.name << " " << a.weight << " "<< a.with_nuts;
	if (a.with_nuts == 1) cout << " s orehami"; else cout << "bez orehi" "\n";
	cout << "Shoko "<< b.name << " " << b.weight << " "<< b.with_nuts;
	if (b.with_nuts == 1) cout << " s orehami"; else cout << "bez orehi" "\n";
	cout << "Shoko "<< c.name << " " << c.weight << " "<< c.with_nuts;
	if (c.with_nuts == 1) cout << " s orehami"; else cout << "bez orehi" "\n";

	return 0;

}

