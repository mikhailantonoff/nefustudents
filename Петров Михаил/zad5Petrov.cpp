// ConsoleApplication9.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <iostream>
#include "chocolate.h"


int _tmain(int argc, _TCHAR* argv[])
{
	chocolate x[4];
		x[0].name="snickers";
		x[0].weight=57;
		x[0].with_nuts=1;
		x[1].name="twix";
		x[1].weight=55;
		x[1].with_nuts=0;
		x[2].name="mars";
		x[2].weight=50;
		x[2].with_nuts=0;
		x[3].name="picnic";
		x[3].weight=38;
		x[3].with_nuts=1;

		for (int i=0; i<4; i++)
		{
			std::cout << x[i].name;
			std::cout << " ";
			std::cout << x[i].weight;
			std::cout << " ";
			if (x[i].with_nuts==0)
			std::cout<<"no nuts \n";
		else
			std::cout<<"with nuts \n";
		}
		system("pause");
		return 0;
}