// ConsoleApplication17.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include "chocolate.h"



using namespace std;


void Chocolates(chocolate * nameChoco, const char * name, int weight, bool with_nuts);
void ChocolatesCout(chocolate * nameChoco);

void main()
{
	chocolate Mars;
	chocolate Snickers;
	chocolate Twix;

	Chocolates(&Mars, "Mars", 200, false);
	Chocolates(&Snickers, "Snickers", 350, true);
	Chocolates(&Twix, "Twix", 150, false);

	ChocolatesCout(&Mars);
	ChocolatesCout(&Snickers);
	ChocolatesCout(&Twix);
}

void Chocolates(chocolate * nameChoco, const char * name, int weight, bool with_nuts)
{
	nameChoco->name = name;
	nameChoco->weight = weight;
	nameChoco->with_nuts = with_nuts;
}

void ChocolatesCout(chocolate * nameChoco)
{
	string withNuts;
	withNuts = nameChoco->with_nuts == true ? "WhitNuts" : "NotNuts";
	cout << nameChoco->name << " " << nameChoco->weight << " " << withNuts << '\n';
}