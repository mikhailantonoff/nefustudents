#include "stdafx.h"
#include "chocolate.h"
#include <stdio.h>
#include <iostream>

int main()
{
	chocolate x,y,z;
	
	x.name="mars";
	x.weight=200;
	x.with_nuts=false;

	y.name="snickers";
	y.weight=300;
	y.with_nuts=true;

	z.name="twix";
	z.weight=250;
	z.with_nuts=false;

	cout<<"name="<<x.name <<" weight=" <<x.weight <<" nuts=";
	if (x.with_nuts==1) cout << "s orehami"; else cout<<"bez orekh"<<endl;

	cout<<"name="<<y.name <<" weight=" <<y.weight <<" nuts=";
	if (y.with_nuts==1) cout << "s orehami"; else cout<<"bez orekh"<<endl;

	cout<<"name="<<z.name <<" weight=" <<z.weight <<" nuts=";
	if (z.with_nuts==1) cout << "s orehami"; else cout<<"bez orekh"<<endl;

	system("pause");
}

